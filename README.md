## Para arrancar el proyecto
Dentro de la carpeta del proyecto ejecutamos en la consola **./vendor/bin/sail up**

## Archivo postman con las rutas de las APIs
[Archivo](https://bitbucket.org/doellojm/example-app/src/60b726299fe2/User%20and%20WorkEntry%20Api.postman_collection.json?at=master)

## Comando para ejecutar los test
**php artisan test**

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
