<?php

namespace App\Http\Controllers;

use App\Models\WorkEntry;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;

class WorkEntryController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return json
     */
    public function soft_delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|int',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $workEntry = WorkEntry::find($request->input('id'));
        if (!$workEntry) {
            return response()->json('invalid id', 400);
        }

        $workEntry->fill([
            'deletedAt' => DB::raw('CURRENT_TIMESTAMP'),
        ]);

        // Save workEntry to database
        $workEntry->save();

        return response()->json('successful', 201);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|int',
            'startDate' => 'required|date_format:Y-m-d H:i:s',
            'endDate' => 'required|after:startDate',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $userId = (int)$request->input('userId');
        $user = User::find($userId);
        if (empty($user) || !empty($user->deletedAt)) {
            return response()->json('invalid user', 400);
        }

        $workEntry = WorkEntry::create([
            'userId' => $user->id,
            'startDate' => $request->input('startDate'),
            'endDate' => $request->input('endDate'),
        ]);

        return response()->json($workEntry, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|int',
            'userId' => 'required|int',
            'startDate' => 'required|date_format:Y-m-d H:i:s',
            'endDate' => 'required|after:startDate',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $userId = (int)$request->input('userId');
        $user = User::find($userId);
        if (!$user || !empty($user->deletedAt)) {
            return response()->json('invalid user', 400);
        }

        $workEntry = WorkEntry::find($request->input('id'));

        $workEntry->fill([
            'userId' => $user->id,
            'startDate' => $request->input('startDate'),
            'endDate' => $request->input('endDate'),
        ]);

        // Save workEntry to database
        $workEntry->save();

        return response()->json($workEntry, 201);
    }

    /**
     * Get workEntry by id in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function get_workEntry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|int',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $workEntry = WorkEntry::find($request->input('id'));

        return response()->json($workEntry, 201);
    }

    /**
     * Get workEntries by id in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function get_workEntries_by_userId(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|int',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $workEntries = DB::table('workEntry')
            ->where('userId', $request->input('userId'))
            ->get();

        return response()->json($workEntries, 201);
    }
}
