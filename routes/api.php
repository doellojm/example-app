<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
}); */

//User API
Route::post('create_user', 'App\Http\Controllers\UserController@create');
Route::put('update_user', 'App\Http\Controllers\UserController@update');
Route::post('delete_user', 'App\Http\Controllers\UserController@soft_delete');
Route::get('get_user', 'App\Http\Controllers\UserController@get_user');
Route::get('get_all_users', 'App\Http\Controllers\UserController@get_all_users');

//WorkEntry APY
Route::post('create_workEntry', 'App\Http\Controllers\WorkEntryController@create');
Route::put('update_workEntry', 'App\Http\Controllers\WorkEntryController@update');
Route::post('delete_workEntry', 'App\Http\Controllers\WorkEntryController@soft_delete');
Route::get('get_workEntry', 'App\Http\Controllers\WorkEntryController@get_workEntry');
Route::get('get_workEntries_by_userId', 'App\Http\Controllers\WorkEntryController@get_workEntries_by_userId');

Route::post('login', 'App\Http\Controllers\UserController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function () {

    Route::post('user', 'App\Http\Controllers\UserController@getAuthenticatedUser');
});
