<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Feature\Str;

use App\Models\User;

class WorkEntryTest extends TestCase
{
    public function test_get_workEntries_by_userId()
    {
        $response = $this->get('/api/get_workEntries_by_userId', ['userId' => 5]);
        $response->assertStatus(201);
    }

    public function test_get_workEntry()
    {
        $response = $this->get('/api/get_workEntry', ['id' => 22]);
        $response->assertStatus(201);
    }

    public function test_update_workEntry()
    {
        $data = [
            'id' => 1,
            'userId' => 6,
            'startDate' => "2021-11-22 02:20:10",
            'endDate' => "2021-11-22 02:50:00",
        ];

        $response = $this->put('/api/update_workEntry', $data);
        $response->assertStatus(201);
    }

    public function test_create_workEntry()
    {
        $data = [
            'userId' => 6,
            'startDate' => "2021-11-22 02:20:10",
            'endDate' => "2021-11-22 02:50:00",
        ];

        $response = $this->post('/api/create_workEntry', $data);
        $response->assertStatus(201);
    }

    public function test_soft_delete()
    {
        $data = [
            'id' => 1,
        ];

        $response = $this->post('/api/delete_workEntry', $data);
        $response->assertStatus(201);
    }
}
